<?php

namespace DTuX\SampleModule\Model;

use \Magento\Framework\Model\AbstractModel;

class Item extends AbstractModel
{


    /**
     * Initialize resource model
     * @return void
     */
    public function _construct()
    {
        $this->_init('DTuX\SampleModule\Model\ResourceModel\Item');
    }


}

