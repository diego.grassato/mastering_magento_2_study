<?php

namespace DTuX\SampleModule\Model\ResourceModel\Item;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('DTuX\SampleModule\Model\Item', 'DTuX\SampleModule\Model\ResourceModel\Item');
    }
}
