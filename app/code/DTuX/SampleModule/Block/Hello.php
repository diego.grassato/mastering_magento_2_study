<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 16/09/18
 * Time: 17:13
 */

namespace DTuX\SampleModule\Block;

use Magento\Framework\View\Element\Template;
use DTuX\SampleModule\Model\ResourceModel\Item\Collection;
use DTuX\SampleModule\Model\ResourceModel\Item\CollectionFactory;

class Hello extends Template
{
    private $collectionFactory;

    public function __construct(
        Template\Context $context,
        CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return \DTuX\SampleModule\Model\Item[]
     */
    public function getItems()
    {
        return $this->collectionFactory->create()->getItems();
    }
}